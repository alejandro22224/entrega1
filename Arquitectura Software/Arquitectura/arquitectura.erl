%%%%%%%%%%Este programa es un ejemplo de la arquitectectura lider/trabajadores
%%%%%%%%El programa compila todos los archivos presentes en la ruta del directorio
%%%%%%%%que el usuario introduce usando los trabajadores que el usuario quiera


-module(arquitectura).
-export([compile/2,init/2,loop/0]).

%%%Construir la ruta completa del archivo con el que trabaja los procesos trabajadores
construir_ruta(Ruta,Filename) ->
	list_to_atom(atom_to_list(Ruta) ++ "/" ++ Filename).

%%%Destruccion de todos los procesos
eliminar([]) -> maestro ! stop;
eliminar([H|T]) -> H ! stop, eliminar(T).

%%%Crear todos los procesos
crear(0)-> [spawn(?MODULE,loop,[])];
crear(Procs)-> [spawn(?MODULE,loop,[])|crear(Procs-1)].

%%%Eliminar de la lista de procesos trabajando el Pid indicado
sacar_Pid(Pid,L) -> sacar(Pid,L,[]).
sacar(_Pid,[],L) -> L;
sacar(Pid,[Pid|T],L) -> T++L;
sacar(Pid,[H|T],L) -> sacar(Pid,T,[H]++L).

%%%Funcion principal del programa
compile(Ruta,NProcs) ->
	register(maestro,spawn(?MODULE,init,[Ruta,NProcs])).


%%%Inicializar el lider
init(_Ruta,0)-> 'Impossible trabajar sin sclavos';
init(Ruta,NProcs)-> 
    Archivos=file:list_dir_all(Ruta),
    case Archivos of
	{error, enoent} -> 'Directory doesn´t exists';
	{error, eacces} -> 'Missing search permissions';
	{ok, Trabajos} -> case Trabajos of
			    [] -> no_work_to_do, maestro ! stop, loopr();  
           	            _ -> loopm(Ruta,crear(NProcs),[],Trabajos)
			end
end.

%%%Final del maestro
loopr() ->
	receive
		stop -> ok
	end.

%%%Logica del maestro
loopm(Ruta,POciosos,[],[]) ->
	eliminar(POciosos), loopr();
loopm(Ruta,[],PTrabajando,Trabajos) ->
	receive
		{Pid,Resultado,Ruta} -> 
			%Mensaje "Resultado" de salida con lo que paso,
			loopm(Ruta,[Pid],sacar_Pid(Pid,PTrabajando),Trabajos)
	end;
loopm(Ruta,POciosos,PTrabajando,[]) ->
	receive
		{Pid,Resultado,Ruta} -> 
			%Mensaje "Resultado" de salida con lo que paso,
			loopm(Ruta,[Pid]++POciosos,sacar_Pid(Pid,PTrabajando),[]) 
	end;
loopm(Ruta,POciosos,PTrabajando,Trabajos) -> 
	{R,I}=file:read_file_info(construir_ruta(Ruta,hd(Trabajos)),
	case {R,I} of 
	   {error,_Reason} -> loopm(Ruta,POciosos,PTrabajando,tl(Trabajos));
			%Pintar la "Reason" 
	   {ok,_} -> case I#file_info.type of
			regular -> hd(POciosos) ! (construir_ruta(hd(Trabajos))),
 				   loopm(Ruta,tl(POciosos),[hd(POciosos)]++PTrabajando,tl(Trabajos));
			_ -> loopm(Ruta,POciosos,PTrabajando,tl(Trabajos))
		     end
	end
end.
	



%%%%Funcion de los trabajadores
loop() ->
	receive
		stop -> ok;
		Ruta -> %compilar el archivo y enviar la referencia al proceso y el resulttado,
			loop()
	end.



