
-module(create).

-export([create/1, reverse_create/1]).

create(N) -> aux (N,1).

aux(N,C) ->
	if N<C ->[];
		true->[C|aux(N,C+1)]
end. 

reverse_create(0)->[];
reverse_create(N) -> [N|reverse_create(N-1)].
