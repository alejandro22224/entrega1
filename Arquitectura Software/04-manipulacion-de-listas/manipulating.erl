
-module(manipulating).

-export([reverse/1, filter/2, concatenate/1, flatten/1]).

reverse(L)->rev(L,[]).
rev(L,Aux)-> case L of
	[]->Aux;
	[H|T]->rev(T,[H|Aux])
end.

filter(L,N) -> [X || X <- L, X =< N].

append(L1,L2)-> case L1 of
	[]->L2;
	[H|T]->append(T,[H|L2])
end.

concatenate(L)->case L of
	[]->[];
	[H|T]-> append(reverse(H), concatenate(T))
end.


flatten(L)->reverse(flatten(L,[])).

flatten([],Acc)->Acc;
flatten([H|T],Acc) when is_list(H) -> flatten(T,flatten(H,Acc));
flatten([H|T],Acc)->flatten(T,[H|Acc]).

