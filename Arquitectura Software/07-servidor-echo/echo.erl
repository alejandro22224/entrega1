
-module(echo).

-export([start/0, stop/0, print/1]).

-export([loop/0]).

start() ->
	register(echo,spawn(?MODULE, loop, [])),ok.

loop() ->
	receive
		{Pid,stop} -> Pid ! {done};
		{Pid,Msg} -> io:format("~p~n",[Msg]), Pid ! {done}, loop()
	end.

stop() ->	
	echo ! {self(),stop},
	receive
		{done}->ok
	end.

print(Term) ->
	echo ! {self(),Term},
	receive
		{done} -> ok
	end.
	 
	
