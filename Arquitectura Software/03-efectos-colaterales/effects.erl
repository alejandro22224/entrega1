
-module(effects).

-export([print/1,even_print/1]).

print(N)-> auxu(N,1).

auxu(N,C)->
	if N==C -> io:format(integer_to_list(C));
	true -> io:format(integer_to_list(C)),
		io:format(","), auxu(N,C+1)
end. 

even_print(N)-> auxd(N,1).


auxd(N,C)->
	if C > N -> ok;
	((C rem 2) == 0) -> io:format(integer_to_list(C)),
			io:format(","), auxd(N,C+2);
	C < N -> auxd(N,C+1);
	true -> ok
end.

