
-module(db).
-export([new/0, write/3, delete/2, read/2, match/2, destroy/1]).

%*************************************
append([],L2)->L2;
append([H|T],L2)->append(T,[H|L2]).

%************************************
new() -> [].

write(Key, Element, []) -> [{Key,Element}];
write(Key, Element, Db) -> [{Key,Element}|Db].

delete(Key,Db)->delete(Key,Db,[]).

delete(_Key, [], L) -> append(L,[]);
delete(Key, [{Key,_Value}|T],L) -> append(append(T,L),[]);
delete(Key, [{Clave,Value}|T], L) -> delete(Key,T,[{Clave,Value}|L]). 

%La prueba no funciona en leer un diccionario sin datos "creo", las pruebas 
%asumen que un diccionario vacio es una lista vacia.
read(_Key, []) -> {error,instance};
read(Key, [{Clave,Value}|[]]) when Clave==Key -> {ok,Value};
read(_Key, [{_Clave,_Value}|[]]) -> {error, instance};
read(Key, [{Clave,Value}|T]) ->  
	if Key==Clave -> {ok,Value};
	true -> read(Key, T)
end. 

match(Element, Db) -> match (Element, Db, []).

match(_Element, [], L) -> L;
match(Element, [{Clave,Value}|T], L) ->
	if Element==Value -> match(Element, T, [Clave|L]);
	true -> match(Element, T, L)
end.


destroy(Diccionario) -> case is_list(Diccionario) of
	false->{error, no_diccionario};
	true->ok
end.
	


