

-module(crossring).
-export([start/3]).

-export([creador/1,init/4,loopr/3,loop/1]).

start(ProcNum, MsgNum, Message) when ProcNum < 3 -> ring:start(ProcNum,MsgNum, Message);
start(ProcNum, MsgNum, Message) ->
	register(centro,spawn(?MODULE, init, 
					[spawn(?MODULE, creador, [ProcNum div 2])
					,spawn(?MODULE, creador, [(ProcNum - ProcNum div 2 - 1)]),
					MsgNum,Message])),
        ok.

creador(1)->loop(centro);
creador(Proc) ->
	Siguiente = spawn(?MODULE, creador, [Proc-1]),
	loop(Siguiente).
	
init(S,M,0,_Msg) ->
	S ! {stop,a}, loopr(S,M,0);
init(S,M,N,Msg) ->
	S ! {Msg,a}, loopr(S,M,N).  %N es el numero de mensages, veces a recorrer el anillo


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

loopr(Siguiente,Medio,1) ->
	receive
		{Msg,a} -> Medio ! {Msg,b}, loopr(Siguiente,Medio,1);
		{b,_Msg} -> Siguiente ! {stop,a}, Medio ! {stop,b}
	end;
loopr(Siguiente,Medio,N) ->
	receive
		{b,Msg} -> Siguiente ! {Msg,a}, loopr(Siguiente,Medio,N-1);		
		{Msg,a} -> Medio ! {Msg,b}, loopr(Siguiente,Medio,N)
	end.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

loop(centro) ->
	case whereis(centro) of
		undefined -> timer:sleep(100);
		_ -> receive
			{stop,_} -> ok;
			{Msg,b} -> centro ! {b,Msg}, loop(centro);
			{Msg,a} -> centro ! {Msg,a}, loop(centro)
		     end	       
        end;
	
loop(Siguiente) ->
	receive
		{stop,Parte} -> Siguiente ! {stop,Parte};
		{Msg,Parte} -> Siguiente ! {Msg,Parte}, loop(Siguiente)
	end.
