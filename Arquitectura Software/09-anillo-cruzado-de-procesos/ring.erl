-module(ring).
-export([start/3]).

-export([creador/1,loopm/3]).

start(0, _MsgNum, _Message) -> numero_de_procesos_0;
start(1, MsgNum, Message) ->
	register(maestro,spawn(?MODULE, loopm, [maestro,MsgNum,Message])), ok;
start(ProcNum, MsgNum, Message) ->
	Siguiente = spawn(?MODULE, creador, [ProcNum-2]),
	register(maestro,spawn(?MODULE, loopm, [Siguiente,MsgNum,Message])),
        ok.

creador(0)->loop(maestro);
creador(Proc) ->
	Siguiente = spawn(?MODULE, creador, [Proc-1]),
	loop(Siguiente).


%************************************************************************************%	
loopm(maestro,0,_Msg) ->
	maestro ! {stop,a}, loopr(maestro,1);
loopm(Siguiente,0,_Msg) ->
	Siguiente ! {stop,a};
loopm(maestro,N,Msg) ->
	maestro ! {b,Msg}, loopr(maestro,N);
loopm(Siguiente,N,Msg) ->
	Siguiente ! Msg, loopr(Siguiente,N).
%***********************************************************************************%
loopr(Siguiente,1)->
	receive
		{stop,a} -> ok;
		{_,_Msg} -> Siguiente ! {stop,a}%, timer:sleep(100) 
	end;
loopr(Siguiente,N) ->
	receive
		{a,Msg} -> Siguiente ! Msg, loopr(Siguiente,N-1);
		{b,Msg} -> Siguiente ! {b,Msg}, loopr(Siguiente,N-1)
	end.
%***********************************************************************************%

loop(maestro) ->
	receive
		{stop,a} -> ok;
		Msg -> case whereis(maestro) of
				undefined -> timer:sleep(100);
				_ -> ok		       
                       end,
                       whereis(maestro) ! {a,Msg}, loop(maestro)
	end;
loop(Siguiente) ->
	receive
		{stop,a} -> Siguiente ! {stop,a};
		Msg -> Siguiente ! Msg, loop(Siguiente)
	end.
